import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { MsAdalAngular6Module } from 'microsoft-adal-angular6';
import { InsertAuthTokenInterceptor } from './insert-auth-token-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
    ]),
    MsAdalAngular6Module.forRoot({
      tenant: '2a916b80-031c-46f9-8f14-884c576496ca',
      clientId: '1b715bba-f119-4dfa-b494-75866d52283b',
      redirectUri: 'http://localhost:4200',
      endpoints: {
        'http://localhost:4200': '7d436785-d1b9-4557-89cb-9b937f5ef530',
      },
      navigateToLoginRequestUrl: false,
      cacheLocation: 'localStorage',
      postLogoutRedirectUri: 'http://localhost:4200',
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InsertAuthTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
