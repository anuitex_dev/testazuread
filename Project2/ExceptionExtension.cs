﻿using Microsoft.AspNetCore.Builder;

namespace Project2
{
  public static class ExceptionExtension
  {
    public static IApplicationBuilder UseException(this IApplicationBuilder builder)
    {
      return builder.UseMiddleware<ExceptionMiddleware>();
    }
  }
}
